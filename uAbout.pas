unit uAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, FlatButton, StdCtrls, ShellApi;

type
  TFormAbout = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    FlatButton1: TFlatButton;
    Label7: TLabel;
    Bevel1: TBevel;
    Panel1: TPanel;
    Panel2: TPanel;
    Label5: TLabel;
    procedure FlatButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure Panel2Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
    { Public declarations }
  end;

var
  FormAbout: TFormAbout;

implementation

uses uMain;

{$R *.dfm}

procedure TFormAbout.FlatButton1Click(Sender: TObject);
begin
Close;
end;

procedure TFormAbout.FormShow(Sender: TObject);
begin
FormAbout.Left:=FormMain.Left+75;
FormAbout.Top:=FormMain.Top+40;
end;

procedure TFormAbout.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
end;

procedure TFormAbout.Panel1Click(Sender: TObject);
begin
ShellExecute(0,'',pchar('mailto:'+Panel1.Caption+'?subject=HzChanger'),'','',1);
end;

procedure TFormAbout.Panel2Click(Sender: TObject);
begin
ShellExecute(0,'',pchar(Panel2.Caption),'','',1);
end;

end.
