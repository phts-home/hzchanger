object FormValues: TFormValues
  Left = 204
  Top = 365
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsNone
  Caption = 'FormValues'
  ClientHeight = 286
  ClientWidth = 426
  Color = 10862793
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 426
    Height = 286
    Style = bsRaised
  end
  object Label6: TLabel
    Left = 5
    Top = 5
    Width = 416
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Color = 2568758
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 1291762
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object FlatButton2: TFlatButton
    Left = 386
    Top = 3
    Width = 17
    Height = 17
    Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = 'X'
    Color = 10862793
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnClick = FlatButton2Click
    ColorNormalState = 10862793
    ColorPushedState = 14016998
    ButtonType = Border01
    ColorHighLightEnable = True
    MoveOnClick = False
    FontColorNormalState = clBlack
    FontColorHighLight = clBlack
    FontColorPushedState = clBlack
  end
  object ListView1: TListView
    Left = 5
    Top = 25
    Width = 416
    Height = 256
    Columns = <
      item
        Caption = #1043#1083#1091#1073#1080#1085#1072' '#1094#1074#1077#1090#1072', bit'
        Width = 110
      end
      item
        Caption = #1064#1080#1088#1080#1085#1072', Px'
        Width = 75
      end
      item
        Caption = #1042#1099#1089#1086#1090#1072', Px'
        Width = 75
      end
      item
        Caption = #1063#1072#1089#1090#1086#1090#1072' '#1088#1072#1079#1074#1077#1088#1090#1082#1080', Hz'
        Width = 130
      end>
    ColumnClick = False
    Ctl3D = False
    FlatScrollBars = True
    GridLines = True
    ReadOnly = True
    RowSelect = True
    SortType = stBoth
    TabOrder = 1
    ViewStyle = vsReport
    OnClick = ListView1Click
    OnDblClick = ListView1DblClick
  end
end
