unit uHelp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, FlatButton;

type
  TFormHelp = class(TForm)
    Bevel1: TBevel;
    Label5: TLabel;
    Image1: TImage;
    FlatButton1: TFlatButton;
    Memo1: TMemo;
    procedure FlatButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
    { Public declarations }
  end;

var
  FormHelp: TFormHelp;

implementation

uses uMain;

{$R *.dfm}

procedure TFormHelp.FlatButton1Click(Sender: TObject);
begin
Close;
end;

procedure TFormHelp.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
end;

procedure TFormHelp.FormShow(Sender: TObject);
begin
FormHelp.Left:=FormMain.Left+75;
FormHelp.Top:=FormMain.Top+10;
end;

end.
