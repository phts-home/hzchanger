unit uValues;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, FlatButton, StdCtrls, ComCtrls;

type
  TFormValues = class(TForm)
    Label6: TLabel;
    FlatButton2: TFlatButton;
    ListView1: TListView;
    Bevel1: TBevel;
    procedure FormShow(Sender: TObject);
    procedure FlatButton2Click(Sender: TObject);
    procedure ListView1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    Modes: array [0..255] of TDevMode;
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
    { Public declarations }
  end;

var
  FormValues: TFormValues;

implementation

uses uMain;

{$R *.dfm}

procedure TFormValues.FormShow(Sender: TObject);
var i: Integer;
begin
i:=0;
ListView1.Clear;
while ENumDisplaySettings(nil, i, Modes[i]) do
  begin
  with ListView1.Items.Add do
    begin
    Caption:=IntToStr(Modes[i].dmBitsPerPel);
    SubItems.Add(IntToStr(Modes[i].dmPelsWidth));
    SubItems.Add(IntToStr(Modes[i].dmPelsHeight));
    SubItems.Add(IntToStr(Modes[i].dmDisplayFrequency));
    end;
  Inc(i);
  end;
end;

procedure TFormValues.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
end;

procedure TFormValues.FlatButton2Click(Sender: TObject);
begin
Close;
end;

procedure TFormValues.ListView1Click(Sender: TObject);
begin
{with ListView1.Items[ListView1.ItemIndex] do
  begin
  FormMain.Edit3.Text:=Caption;
  FormMain.Edit5.Text:=SubItems[0];
  FormMain.Edit6.Text:=SubItems[1];
  FormMain.Edit1.Text:=SubItems[2];
  end;               }
end;

procedure TFormValues.FormCreate(Sender: TObject);
begin
FormValues.Left:=FormMain.Left-136;
FormValues.Top:=FormMain.Top+120;
end;

procedure TFormValues.ListView1DblClick(Sender: TObject);
begin
with ListView1.Items[ListView1.ItemIndex] do
  begin
  FormMain.Edit3.Text:=Caption;
  FormMain.Edit5.Text:=SubItems[0];
  FormMain.Edit6.Text:=SubItems[1];
  FormMain.Edit1.Text:=SubItems[2];
  end;
//FormMain.FlatButton_applyMouseDown(FormMain.FlatButton_apply,mbLeft,[],10,10);
end;

end.
