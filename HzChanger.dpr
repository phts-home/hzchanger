program HzChanger;

uses
  Forms,
  uMain in 'uMain.pas' {FormMain},
  uAbout in 'uAbout.pas' {FormAbout},
  uHelp in 'uHelp.pas' {FormHelp},
  uValues in 'uValues.pas' {FormValues};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'HzChanger';
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormAbout, FormAbout);
  Application.CreateForm(TFormHelp, FormHelp);
  Application.CreateForm(TFormValues, FormValues);
  Application.Run;
end.
