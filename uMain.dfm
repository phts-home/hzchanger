object FormMain: TFormMain
  Left = 340
  Top = 234
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsNone
  Caption = 'HzChanger'
  ClientHeight = 251
  ClientWidth = 424
  Color = 10862793
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 2
    Top = 24
    Width = 420
    Height = 207
  end
  object Label1: TLabel
    Left = 280
    Top = 39
    Width = 13
    Height = 13
    Caption = 'Hz'
  end
  object Label2: TLabel
    Left = 65
    Top = 39
    Width = 11
    Height = 13
    Caption = 'bit'
  end
  object Label3: TLabel
    Left = 150
    Top = 37
    Width = 5
    Height = 13
    Caption = 'x'
  end
  object Label4: TLabel
    Left = 65
    Top = 154
    Width = 21
    Height = 13
    Caption = #1089#1077#1082'.'
    ParentShowHint = False
    ShowHint = False
  end
  object Label5: TLabel
    Left = 15
    Top = 125
    Width = 39
    Height = 13
    Caption = #1058#1072#1081#1084#1077#1088
  end
  object Label6: TLabel
    Left = 2
    Top = 5
    Width = 420
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'HzChanger'
    Color = 2568758
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 1291762
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label7: TLabel
    Left = 65
    Top = 74
    Width = 21
    Height = 13
    Caption = #1089#1077#1082'.'
    ParentShowHint = False
    ShowHint = False
  end
  object Panel1: TPanel
    Left = 60
    Top = 135
    Width = 346
    Height = 1
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = 'Panel1'
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 8
  end
  object FlatButton_apply: TFlatButton
    Left = 310
    Top = 35
    Width = 95
    Height = 19
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
    Color = 10862793
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 5
    OnMouseDown = FlatButton_applyMouseDown
    ColorNormalState = 10862793
    ColorPushedState = 14016998
    ButtonType = Border01
    ColorHighLightEnable = True
    MoveOnClick = False
    FontColorNormalState = clMaroon
    FontColorHighLight = clMaroon
    FontColorPushedState = clMaroon
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 232
    Width = 424
    Height = 19
    BorderWidth = 2
    Color = 10862793
    Panels = <
      item
        Text = #1058#1072#1081#1084#1077#1088': '#1074#1099#1082#1083'.'
        Width = 110
      end
      item
        Text = #1057#1083#1077#1078#1077#1085#1080#1077': '#1074#1099#1082#1083'.'
        Width = 110
      end
      item
        Alignment = taCenter
        Text = '32 bit'
        Width = 50
      end
      item
        Alignment = taCenter
        Text = '1024'
        Width = 50
      end
      item
        Alignment = taCenter
        Text = '768'
        Width = 50
      end
      item
        Alignment = taCenter
        Text = '85 Hz'
        Width = 50
      end>
  end
  object FlatButton_start: TFlatButton
    Left = 310
    Top = 150
    Width = 95
    Height = 19
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1057#1090#1072#1088#1090'/'#1057#1090#1086#1087
    Color = 10862793
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 6
    OnMouseDown = FlatButton_startMouseDown
    ColorNormalState = 10862793
    ColorPushedState = 14016998
    ButtonType = Border01
    ColorHighLightEnable = True
    MoveOnClick = False
    FontColorNormalState = clGreen
    FontColorHighLight = clGreen
    FontColorPushedState = clGreen
  end
  object CheckBox1: TCheckBox
    Left = 15
    Top = 200
    Width = 231
    Height = 17
    Caption = #1047#1072#1082#1088#1099#1090#1100' '#1087#1088#1086#1075#1088#1072#1084#1084#1091' '#1087#1086#1089#1083#1077' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103
    Checked = True
    Ctl3D = True
    ParentCtl3D = False
    State = cbChecked
    TabOrder = 3
  end
  object CheckBox2: TCheckBox
    Left = 15
    Top = 180
    Width = 231
    Height = 17
    Caption = #1057#1074#1077#1088#1085#1091#1090#1100' '#1087#1086#1089#1083#1077' '#1079#1072#1087#1091#1089#1082#1072
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
  object Edit1: TEdit
    Left = 230
    Top = 35
    Width = 31
    Height = 19
    Hint = #1063#1072#1089#1090#1086#1090#1072' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103' '#1101#1082#1088#1072#1085#1072
    Color = clWhite
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Text = '85'
    OnChange = Edit1Change
    OnClick = Edit1Click
    OnKeyPress = Edit1KeyPress
  end
  object FlatButton4: TFlatButton
    Left = 260
    Top = 35
    Width = 15
    Height = 10
    Hint = #1063#1072#1089#1090#1086#1090#1072' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103' '#1101#1082#1088#1072#1085#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
    OnMouseDown = FlatButton4MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object FlatButton5: TFlatButton
    Left = 260
    Top = 44
    Width = 15
    Height = 10
    Hint = #1063#1072#1089#1090#1086#1090#1072' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103' '#1101#1082#1088#1072#1085#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 10
    OnMouseDown = FlatButton5MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object Edit2: TEdit
    Left = 15
    Top = 150
    Width = 31
    Height = 19
    Hint = #1048#1085#1090#1077#1088#1074#1072#1083' '#1090#1072#1081#1084#1077#1088#1072
    Color = clWhite
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    Text = '30'
    OnChange = Edit2Change
    OnKeyPress = Edit2KeyPress
  end
  object FlatButton6: TFlatButton
    Left = 45
    Top = 150
    Width = 15
    Height = 10
    Hint = #1048#1085#1090#1077#1088#1074#1072#1083' '#1090#1072#1081#1084#1077#1088#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 11
    OnMouseDown = FlatButton6MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object FlatButton7: TFlatButton
    Left = 45
    Top = 159
    Width = 15
    Height = 10
    Hint = #1048#1085#1090#1077#1088#1074#1072#1083' '#1090#1072#1081#1084#1077#1088#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 12
    OnMouseDown = FlatButton7MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object Edit3: TEdit
    Left = 15
    Top = 35
    Width = 31
    Height = 19
    Hint = #1043#1083#1091#1073#1080#1085#1072' '#1094#1074#1077#1090#1072
    Color = clWhite
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Text = '32'
    OnChange = Edit1Change
    OnClick = Edit1Click
    OnKeyPress = Edit1KeyPress
  end
  object FlatButton8: TFlatButton
    Left = 45
    Top = 35
    Width = 15
    Height = 10
    Hint = #1043#1083#1091#1073#1080#1085#1072' '#1094#1074#1077#1090#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 13
    OnMouseDown = FlatButton8MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object FlatButton9: TFlatButton
    Left = 45
    Top = 44
    Width = 15
    Height = 10
    Hint = #1043#1083#1091#1073#1080#1085#1072' '#1094#1074#1077#1090#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 14
    OnMouseDown = FlatButton9MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object FlatButton1: TFlatButton
    Left = 369
    Top = 3
    Width = 17
    Height = 17
    Hint = #1057#1074#1077#1088#1085#1091#1090#1100' '#1074' '#1090#1088#1077#1081
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = '_'
    Color = 10862793
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 15
    OnClick = FlatButton1Click
    ColorNormalState = 10862793
    ColorPushedState = 14016998
    ButtonType = Border01
    ColorHighLightEnable = True
    MoveOnClick = False
    FontColorNormalState = clBlack
    FontColorHighLight = clBlack
    FontColorPushedState = clBlack
  end
  object FlatButton2: TFlatButton
    Left = 389
    Top = 3
    Width = 17
    Height = 17
    Hint = #1047#1072#1082#1088#1099#1090#1100' '#1087#1088#1086#1075#1088#1072#1084#1084#1091
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = 'X'
    Color = 10862793
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 16
    OnClick = FlatButton2Click
    ColorNormalState = 10862793
    ColorPushedState = 14016998
    ButtonType = Border01
    ColorHighLightEnable = True
    MoveOnClick = False
    FontColorNormalState = clBlack
    FontColorHighLight = clBlack
    FontColorPushedState = clBlack
  end
  object FlatButton3: TFlatButton
    Left = 310
    Top = 70
    Width = 95
    Height = 19
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1057#1083#1077#1076#1080#1090#1100
    Color = 10862793
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 17
    OnMouseDown = FlatButton3MouseDown
    ColorNormalState = 10862793
    ColorPushedState = 14016998
    ButtonType = Border01
    ColorHighLightEnable = True
    MoveOnClick = False
    FontColorNormalState = clNavy
    FontColorHighLight = clNavy
    FontColorPushedState = clNavy
  end
  object Edit4: TEdit
    Left = 15
    Top = 70
    Width = 31
    Height = 19
    Hint = #1048#1085#1090#1077#1088#1074#1072#1083' '#1084#1077#1078#1076#1091' '#1087#1088#1086#1074#1077#1088#1082#1072#1084#1080
    Color = clWhite
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 18
    Text = '10'
    OnChange = Edit1Change
    OnKeyPress = Edit2KeyPress
  end
  object FlatButton10: TFlatButton
    Left = 45
    Top = 70
    Width = 15
    Height = 10
    Hint = #1048#1085#1090#1077#1088#1074#1072#1083' '#1084#1077#1078#1076#1091' '#1087#1088#1086#1074#1077#1088#1082#1072#1084#1080
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 19
    OnMouseDown = FlatButton10MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object FlatButton11: TFlatButton
    Left = 45
    Top = 79
    Width = 15
    Height = 10
    Hint = #1048#1085#1090#1077#1088#1074#1072#1083' '#1084#1077#1078#1076#1091' '#1087#1088#1086#1074#1077#1088#1082#1072#1084#1080
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 20
    OnMouseDown = FlatButton11MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object FlatButton12: TFlatButton
    Left = 349
    Top = 3
    Width = 17
    Height = 17
    Hint = #1057#1087#1088#1072#1074#1082#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = '?'
    Color = 10862793
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 21
    OnClick = FlatButton12Click
    ColorNormalState = 10862793
    ColorPushedState = 14016998
    ButtonType = Border01
    ColorHighLightEnable = True
    MoveOnClick = False
    FontColorNormalState = clBlack
    FontColorHighLight = clBlack
    FontColorPushedState = clBlack
  end
  object FlatButton13: TFlatButton
    Left = 15
    Top = 3
    Width = 17
    Height = 17
    Hint = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = '@'
    Color = 10862793
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 22
    OnClick = FlatButton13Click
    ColorNormalState = 10862793
    ColorPushedState = 14016998
    ButtonType = Border01
    ColorHighLightEnable = True
    MoveOnClick = False
    FontColorNormalState = clBlack
    FontColorHighLight = clBlack
    FontColorPushedState = clBlack
  end
  object FlatButton14: TFlatButton
    Left = 130
    Top = 35
    Width = 15
    Height = 10
    Hint = #1064#1080#1088#1080#1085#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 23
    OnMouseDown = FlatButton14MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object FlatButton15: TFlatButton
    Left = 130
    Top = 44
    Width = 15
    Height = 10
    Hint = #1064#1080#1088#1080#1085#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 24
    OnMouseDown = FlatButton15MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object Edit5: TEdit
    Left = 100
    Top = 35
    Width = 31
    Height = 19
    Hint = #1064#1080#1088#1080#1085#1072
    Color = clWhite
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 25
    Text = '1024'
    OnChange = Edit1Change
    OnClick = Edit1Click
    OnKeyPress = Edit1KeyPress
  end
  object Edit6: TEdit
    Left = 160
    Top = 35
    Width = 31
    Height = 19
    Hint = #1042#1099#1089#1086#1090#1072
    Color = clWhite
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 26
    Text = '768'
    OnChange = Edit1Change
    OnClick = Edit1Click
    OnKeyPress = Edit1KeyPress
  end
  object FlatButton16: TFlatButton
    Left = 190
    Top = 35
    Width = 15
    Height = 10
    Hint = #1042#1099#1089#1086#1090#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 27
    OnMouseDown = FlatButton16MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object FlatButton17: TFlatButton
    Left = 190
    Top = 44
    Width = 15
    Height = 10
    Hint = #1042#1099#1089#1086#1090#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 28
    OnMouseDown = FlatButton17MouseDown
    ColorNormalState = clSilver
    ButtonType = Border01
    MoveOnClick = False
  end
  object CheckBox3: TCheckBox
    Left = 15
    Top = 100
    Width = 231
    Height = 17
    Caption = #1057#1074#1077#1088#1085#1091#1090#1100' '#1087#1086#1089#1083#1077' '#1079#1072#1087#1091#1089#1082#1072
    Checked = True
    State = cbChecked
    TabOrder = 29
  end
  object FlatButton18: TFlatButton
    Left = 310
    Top = 3
    Width = 17
    Height = 17
    Hint = #1055#1086#1076#1076#1077#1088#1078#1080#1074#1072#1077#1084#1099#1077' '#1088#1077#1078#1080#1084#1099' '#1076#1083#1103' '#1101#1090#1086#1075#1086' '#1084#1086#1085#1080#1090#1086#1088#1072
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = '%'
    Color = 10862793
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 30
    OnClick = FlatButton18Click
    ColorNormalState = 10862793
    ColorPushedState = 14016998
    ButtonType = Border01
    ColorHighLightEnable = True
    MoveOnClick = False
    FontColorNormalState = clBlack
    FontColorHighLight = clBlack
    FontColorPushedState = clBlack
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 40000
    OnTimer = Timer1Timer
    Left = 270
    Top = 145
  end
  object PopupMenu1: TPopupMenu
    Left = 360
    Top = 170
    object N12: TMenuItem
      Enabled = False
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Caption = #1057#1083#1077#1078#1077#1085#1080#1077
      object N5: TMenuItem
        Caption = #1042#1082#1083#1102#1095#1080#1090#1100
        OnClick = N5Click
      end
      object N6: TMenuItem
        Caption = #1042#1099#1082#1083#1102#1095#1080#1090#1100
        OnClick = N6Click
      end
    end
    object N4: TMenuItem
      Caption = #1058#1072#1081#1084#1077#1088
      object N7: TMenuItem
        Caption = #1042#1082#1083#1102#1095#1080#1090#1100
        OnClick = N7Click
      end
      object N8: TMenuItem
        Caption = #1042#1099#1082#1083#1102#1095#1080#1090#1100
        OnClick = N8Click
      end
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Caption = #1056#1072#1079#1074#1077#1088#1085#1091#1090#1100
      OnClick = N2Click
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Caption = #1047#1072#1082#1088#1099#1090#1100
      OnClick = N1Click
    end
  end
  object Timer_check: TTimer
    Interval = 10000
    OnTimer = Timer_checkTimer
    Left = 390
    Top = 170
  end
  object Timer_sl: TTimer
    Enabled = False
    OnTimer = Timer_slTimer
    Left = 270
    Top = 65
  end
end
