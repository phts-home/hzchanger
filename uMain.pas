unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls, shellAPI, FlatButton, ComCtrls, Menus, IniFiles;

const
 WM_MYICONNOTIFY = WM_USER + 123;

type
  TFormMain = class(TForm)
    Timer1: TTimer;
    Label1: TLabel;
    Label2: TLabel;
    FlatButton_apply: TFlatButton;
    Label3: TLabel;
    StatusBar1: TStatusBar;
    FlatButton_start: TFlatButton;
    Label4: TLabel;
    CheckBox1: TCheckBox;
    Label5: TLabel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    CheckBox2: TCheckBox;
    Timer_check: TTimer;
    Panel1: TPanel;
    Edit1: TEdit;
    FlatButton4: TFlatButton;
    FlatButton5: TFlatButton;
    Edit2: TEdit;
    FlatButton6: TFlatButton;
    FlatButton7: TFlatButton;
    Edit3: TEdit;
    FlatButton8: TFlatButton;
    FlatButton9: TFlatButton;
    Label6: TLabel;
    FlatButton1: TFlatButton;
    FlatButton2: TFlatButton;
    Bevel1: TBevel;
    FlatButton3: TFlatButton;
    Edit4: TEdit;
    FlatButton10: TFlatButton;
    FlatButton11: TFlatButton;
    Label7: TLabel;
    Timer_sl: TTimer;
    FlatButton12: TFlatButton;
    FlatButton13: TFlatButton;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    FlatButton14: TFlatButton;
    FlatButton15: TFlatButton;
    Edit5: TEdit;
    Edit6: TEdit;
    FlatButton16: TFlatButton;
    FlatButton17: TFlatButton;
    CheckBox3: TCheckBox;
    FlatButton18: TFlatButton;
    procedure Timer1Timer(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer_checkTimer(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure FlatButton1Click(Sender: TObject);
    procedure FlatButton2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure Timer_slTimer(Sender: TObject);
    procedure FlatButton12Click(Sender: TObject);
    procedure FlatButton13Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure FlatButton18Click(Sender: TObject);
    procedure FlatButton8MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton9MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton14MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton15MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton16MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton17MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton4MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton5MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton10MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton11MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton6MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton7MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton_startMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FlatButton3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FlatButton_applyMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Edit1Change(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
  private
    { Private declarations }
  public
    IniF: TIniFile;
    DC: hDC;
    procedure SetParameters;
    procedure HideMainForm;
    procedure RestoreMainForm;
    procedure CreateTrayIcon;
    procedure DeleteTrayIcon;
    procedure WMICON(var msg: TMessage); message WM_MYICONNOTIFY;
    procedure ShowState;
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

uses uAbout, uHelp, uValues;

{$R *.dfm}

procedure TFormMain.Timer1Timer(Sender: TObject);
begin
SetParameters;
Timer1.Enabled:=False;
if CheckBox1.Checked then
  begin
  DeleteTrayIcon;
  Close;
  end;
end;

procedure TFormMain.SetParameters;
var D: TDevMode;
begin
D.dmFields:=0;
if Edit3.Text<>'-'
  then D.dmBitsPerPel:=StrToInt(Edit3.Text)
  else D.dmBitsPerPel:=GetDeviceCaps(DC, BITSPIXEL);
if Edit5.Text<>'-'
  then D.dmPelsWidth:=StrToInt(Edit5.Text)
  else D.dmPelsWidth:=Screen.Width;
if Edit6.Text<>'-'
  then D.dmPelsHeight:=StrToInt(Edit6.Text)
  else D.dmPelsWidth:=Screen.Height;
if Edit1.Text<>'-'
  then D.dmDisplayFrequency:=StrToInt(Edit1.Text)
  else D.dmDisplayFrequency:=GetDeviceCaps(DC, VREFRESH);
D.dmFields:=D.dmFields+DM_BITSPERPEL;
D.dmFields:=D.dmFields+DM_PELSWIDTH;
D.dmFields:=D.dmFields+DM_PELSHEIGHT;
D.dmFields:=D.dmFields+DM_DISPLAYFREQUENCY;
if D.dmFields<>0 then
  begin
  D.dmSize:=SizeOf(D);
  if ChangeDisplaySettings(D,CDS_TEST)=DISP_CHANGE_SUCCESSFUL
    then ChangeDisplaySettings(D,CDS_UPDATEREGISTRY)
    else Application.MessageBox('������ ��� ��������� ���������� ��������','HzChanger',MB_OK+MB_ICONERROR);
  end;
end;

//---------------

procedure TFormMain.WMICON(var msg: TMessage);
var P : TPoint;
begin
case msg.LParam of
  WM_RBUTTONDOWN:
    begin
    GetCursorPos(p);
    SetForegroundWindow(Application.MainForm.Handle);
    PopupMenu1.Popup(P.X, P.Y);
    end;
  WM_LBUTTONDBLCLK:
    begin
    SetForegroundWindow(Application.MainForm.Handle);
    N2Click(Self);
    end;
  end;
end;

procedure TFormMain.HideMainForm;
begin
  Application.ShowMainForm := False;
  ShowWindow(Application.Handle, SW_HIDE);
  ShowWindow(Application.MainForm.Handle, SW_HIDE);
end;

procedure TFormMain.RestoreMainForm;
var i,j: Integer;
begin
Application.ShowMainForm:=True;
ShowWindow(Application.Handle, SW_RESTORE);
ShowWindow(Application.MainForm.Handle, SW_RESTORE);
for I := 0 to Application.MainForm.ComponentCount -1 do
  if Application.MainForm.Components[I] is TWinControl then
    with Application.MainForm.Components[I] as TWinControl do
      if Visible then
        begin
        ShowWindow(Handle, SW_SHOWDEFAULT);
        for J := 0 to ComponentCount -1 do
          if Components[J] is TWinControl then
            ShowWindow((Components[J] as TWinControl).Handle, SW_SHOWDEFAULT);
        end;
end;

procedure TFormMain.CreateTrayIcon;
var nidata : TNotifyIconData;
begin
 with nidata do
  begin
   cbSize := SizeOf(TNotifyIconData);
   Wnd := Self.Handle;
   uID := 1;
   uFlags := NIF_ICON or NIF_MESSAGE or NIF_TIP;
   uCallBackMessage := WM_MYICONNOTIFY;
   hIcon := Application.Icon.Handle;
   StrPCopy(szTip,pchar(Application.Title));
  end;
  Shell_NotifyIcon(NIM_ADD, @nidata);
N12.Caption:=StatusBar1.Panels[0].Text+' '+StatusBar1.Panels[1].Text;
end;

procedure TFormMain.DeleteTrayIcon;
var nidata : TNotifyIconData;
begin
 with nidata do
  begin
   cbSize := SizeOf(TNotifyIconData);
   Wnd := Self.Handle;
   uID := 1;
  end;
  Shell_NotifyIcon(NIM_DELETE, @nidata);
end;

//--------------

procedure TFormMain.N1Click(Sender: TObject);
begin
DeleteTrayIcon;
Close;
end;

procedure TFormMain.N2Click(Sender: TObject);
begin
RestoreMainForm;
DeleteTrayIcon;
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
IniF:=TIniFile.Create(extractfilepath(paramstr(0))+'HzChanger.ini');
CheckBox1.Checked:=IniF.ReadBool('Options','CloseProg',true);
CheckBox2.Checked:=IniF.ReadBool('Options','Minimize01',true);
CheckBox3.Checked:=IniF.ReadBool('Options','Minimize02',true);
Edit2.Text:=inttostr(IniF.ReadInteger('Options','Timer',30));
Edit4.Text:=inttostr(IniF.ReadInteger('Options','Watch',10));
FormMain.Left:=IniF.ReadInteger('Form','Left',300);
FormMain.Top:=IniF.ReadInteger('Form','Top',286);
DC := CreateDC('DISPLAY', nil, nil, nil);
Edit5.Text:=inttostr(Screen.Width);
Edit6.Text:=inttostr(Screen.Height);
Edit1.Text:=inttostr(GetDeviceCaps(DC, VREFRESH));
Edit3.Text:=inttostr(GetDeviceCaps(DC, BITSPIXEL));
ShowState;
end;

procedure TFormMain.ShowState;
begin
StatusBar1.Panels[2].Text:=inttostr(GetDeviceCaps(DC, BITSPIXEL))+' bit';
StatusBar1.Panels[3].Text:=inttostr(Screen.Width);
StatusBar1.Panels[4].Text:=inttostr(Screen.Height);
StatusBar1.Panels[5].Text:=inttostr(GetDeviceCaps(DC, VREFRESH))+' Hz';
end;

procedure TFormMain.Timer_checkTimer(Sender: TObject);
begin
ShowState;
FlatButton_apply.DownAndHold:=True;
FlatButton_apply.DownAndHold:=False;
FlatButton3.DownAndHold:=True;
FlatButton3.DownAndHold:=False;
FlatButton_start.DownAndHold:=True;
FlatButton_start.DownAndHold:=False;
end;

procedure TFormMain.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if key=#45 then
  begin
  (Sender as TEdit).Text:='-';
  end;
if (Sender as TEdit).Text='-' then (Sender as TEdit).SelectAll;
if ((key<#48)or(key>#57))and(key<>#8) then key:=#0
end;

procedure TFormMain.FlatButton1Click(Sender: TObject);
begin
application.Minimize;
HideMainForm;
CreateTrayIcon;
end;

procedure TFormMain.FlatButton2Click(Sender: TObject);
begin
Close;
end;

procedure TFormMain.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
end;

procedure TFormMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
IniF:=TIniFile.Create(extractfilepath(paramstr(0))+'HzChanger.ini');
IniF.WriteBool('Options','CloseProg',CheckBox1.Checked);
IniF.WriteBool('Options','Minimize01',CheckBox2.Checked);
IniF.WriteBool('Options','Minimize02',CheckBox3.Checked);
IniF.WriteInteger('Options','Timer',StrToInt(Edit2.Text));
IniF.WriteInteger('Options','Watch',StrToInt(Edit4.Text));
IniF.WriteInteger('Form','Left',FormMain.Left);
IniF.WriteInteger('Form','Top',FormMain.Top);
end;

procedure TFormMain.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
if ((key<#48)or(key>#57))and(key<>#8) then key:=#0;
end;

procedure TFormMain.Timer_slTimer(Sender: TObject);
var D: TDevMode;
begin
D.dmFields:=0;
if (Edit3.Text<>'-')and(Edit3.Text<>inttostr(GetDeviceCaps(DC, BITSPIXEL))) then
  begin
  D.dmBitsPerPel:=StrToInt(Edit3.Text);
  D.dmFields:=D.dmFields+DM_BITSPERPEL;
  end;
if (Edit5.Text<>'-')and(Edit5.Text<>inttostr(Screen.Width)) then
  begin
  D.dmPelsWidth:=StrToInt(Edit5.Text);
  D.dmFields:=D.dmFields+DM_PELSWIDTH;
  end;
if (Edit6.Text<>'-')and(Edit6.Text<>inttostr(Screen.Height)) then
  begin
  D.dmPelsHeight:=StrToInt(Edit6.Text);
  D.dmFields:=D.dmFields+DM_PELSHEIGHT;
  end;
if (Edit1.Text<>'-')and(Edit1.Text<>inttostr(GetDeviceCaps(DC, VREFRESH))) then
  begin
  D.dmDisplayFrequency:=StrToInt(Edit1.Text);
  D.dmFields:=D.dmFields+DM_DISPLAYFREQUENCY;
  end;
if D.dmFields<>0 then
  begin
  D.dmSize:=SizeOf(D);
  if ChangeDisplaySettings(D,CDS_TEST)=DISP_CHANGE_SUCCESSFUL then
    ChangeDisplaySettings(D,CDS_UPDATEREGISTRY);
  end; 
end;

procedure TFormMain.FlatButton12Click(Sender: TObject);
begin
FormHelp.ShowModal;
end;

procedure TFormMain.FlatButton13Click(Sender: TObject);
begin
FormAbout.ShowModal;
end;

procedure TFormMain.N7Click(Sender: TObject);
begin
Timer1.Enabled:=false;
Timer1.Interval:=StrToInt(Edit2.Text)*1000;
Timer1.Enabled:=true;
StatusBar1.Panels[0].Text:='������: ���.';
N12.Caption:=StatusBar1.Panels[0].Text+' '+StatusBar1.Panels[1].Text;
end;

procedure TFormMain.N8Click(Sender: TObject);
begin
Timer1.Enabled:=false;
StatusBar1.Panels[0].Text:='������: ����.';
N12.Caption:=StatusBar1.Panels[0].Text+' '+StatusBar1.Panels[1].Text;
end;

procedure TFormMain.N5Click(Sender: TObject);
begin
Timer_sl.Enabled:=false;
Timer_sl.Interval:=StrToInt(Edit4.Text)*1000;
Timer_sl.Enabled:=true;
StatusBar1.Panels[1].Text:='��������: ���.';
FlatButton3.Caption:='����������';
N12.Caption:=StatusBar1.Panels[0].Text+' '+StatusBar1.Panels[1].Text;
end;

procedure TFormMain.N6Click(Sender: TObject);
begin
Timer_sl.Enabled:=false;
StatusBar1.Panels[1].Text:='��������: ����.';
FlatButton3.Caption:='�������';
N12.Caption:=StatusBar1.Panels[0].Text+' '+StatusBar1.Panels[1].Text;
end;

procedure TFormMain.FlatButton18Click(Sender: TObject);
begin
if FlatButton3.Caption = '����������' then
  begin
  FlatButton3MouseDown(FlatButton3,mbLeft,[],0,0);
  end;
if not FormValues.Showing
  then FormValues.Show
  else FormValues.Close;
end;

procedure TFormMain.FlatButton8MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
Edit3.Text:='32';
end;

procedure TFormMain.FlatButton9MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
Edit3.Text:='16';
end;

procedure TFormMain.FlatButton14MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Edit5.Text<>'-' then
  begin
  case StrToInt(Edit5.Text) of
    320: Edit5.Text:='400';
    400: Edit5.Text:='480';
    480: Edit5.Text:='512';
    512: Edit5.Text:='640';
    640: Edit5.Text:='800';
    800: Edit5.Text:='1024';
    1024:Edit5.Text:='1280';
    1280:Edit5.Text:='1280'
      else Edit5.Text:='320';
    end;
  end
  else begin
  Edit5.Text:='320';
  end;
end;

procedure TFormMain.FlatButton15MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Edit5.Text<>'-' then
  begin
  case StrToInt(Edit5.Text) of
    320: Edit5.Text:='320';
    400: Edit5.Text:='320';
    480: Edit5.Text:='400';
    512: Edit5.Text:='480';
    640: Edit5.Text:='512';
    800: Edit5.Text:='640';
    1024:Edit5.Text:='800';
    1280:Edit5.Text:='1024';
      else Edit5.Text:='1280';
    end;
  end
  else begin
  Edit5.Text:='1280';
  end;
end;

procedure TFormMain.FlatButton16MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Edit6.Text<>'-' then
  begin
  case StrToInt(Edit6.Text) of
    200: Edit6.Text:='240';
    240: Edit6.Text:='300';
    300: Edit6.Text:='360';
    360: Edit6.Text:='384';
    384: Edit6.Text:='400';
    400: Edit6.Text:='480';
    480: Edit6.Text:='576';
    576: Edit6.Text:='600';
    600: Edit6.Text:='612';
    612: Edit6.Text:='720';
    720: Edit6.Text:='768';
    768: Edit6.Text:='960';
    960: Edit6.Text:='1024';
    1024: Edit6.Text:='1024';
      else Edit6.Text:='200';
    end;
  end
  else begin
  Edit6.Text:='200';
  end;
end;

procedure TFormMain.FlatButton17MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Edit6.Text<>'-' then
  begin
  case StrToInt(Edit6.Text) of
    200: Edit6.Text:='200';
    240: Edit6.Text:='200';
    300: Edit6.Text:='240';
    360: Edit6.Text:='300';
    384: Edit6.Text:='360';
    400: Edit6.Text:='384';
    480: Edit6.Text:='400';
    576: Edit6.Text:='480';
    600: Edit6.Text:='576';
    612: Edit6.Text:='600';
    720: Edit6.Text:='612';
    768: Edit6.Text:='720';
    960: Edit6.Text:='768';
    1024: Edit6.Text:='960';
      else Edit6.Text:='1024';
    end;
  end
  else begin
  Edit6.Text:='1024';
  end;
end;

procedure TFormMain.FlatButton4MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if (Edit1.Text<>'1')and(Edit1.Text<>'-') then Edit1.Text:=inttostr(StrToInt(Edit1.Text)+5)
  else Edit1.Text:='5';
end;

procedure TFormMain.FlatButton5MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Edit1.Text<>'-' then
  begin
  Edit1.Text:=inttostr(StrToInt(Edit1.Text)-5);
  if StrToInt(Edit1.Text)<=0 then Edit1.Text:='1';
  end;
end;

procedure TFormMain.FlatButton10MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if (Edit4.Text<>'1')and(Edit4.Text<>'') then Edit4.Text:=inttostr(StrToInt(Edit4.Text)+5)
  else Edit4.Text:='5';
end;

procedure TFormMain.FlatButton11MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Edit4.Text<>'' then
  begin
  Edit4.Text:=inttostr(StrToInt(Edit4.Text)-5);
  if StrToInt(Edit4.Text)<=0 then Edit4.Text:='1';
  end;
end;

procedure TFormMain.FlatButton6MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if (Edit2.Text<>'1')and(Edit2.Text<>'') then Edit2.Text:=inttostr(StrToInt(Edit2.Text)+5)
  else Edit2.Text:='5';
end;

procedure TFormMain.FlatButton7MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Edit2.Text<>'' then
  begin
  Edit2.Text:=inttostr(StrToInt(Edit2.Text)-5);
  if StrToInt(Edit2.Text)<=0 then Edit2.Text:='1';
  end;
end;

procedure TFormMain.FlatButton_startMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
Timer1.Interval:=StrToInt(Edit2.Text)*1000;

Timer1.Enabled:=not Timer1.Enabled;
if Timer1.Enabled then
  begin
  StatusBar1.Panels[0].Text:='������: ���.';
  if CheckBox2.Checked then
    begin
    application.Minimize;
    HideMainForm;
    CreateTrayIcon;
    end;
  end else
  begin
  StatusBar1.Panels[0].Text:='������: ����.';
  end;
end;

procedure TFormMain.FlatButton3MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
Timer_sl.Interval:=StrToInt(Edit4.Text)*1000;

Timer_sl.Enabled:=not Timer_sl.Enabled;
if Timer_sl.Enabled then
  begin
  StatusBar1.Panels[1].Text:='��������: ���.';
  FlatButton3.Caption:='����������';
  if CheckBox3.Checked then
    begin
    application.Minimize;
    HideMainForm;
    CreateTrayIcon;
    end;
  end else
  begin
  StatusBar1.Panels[1].Text:='��������: ����.';
  FlatButton3.Caption:='�������';
  end;
end;

procedure TFormMain.FlatButton_applyMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Application.MessageBox('�� ������ ��������� ������ ���������?','HzChanger',MB_YESNO+MB_ICONQUESTION)=IDYES
  then begin
  SetParameters;
  ShowState;
  end;
end;

procedure TFormMain.Edit1Change(Sender: TObject);
begin
if FlatButton3.Caption = '����������' then
  begin
  FlatButton3MouseDown(FlatButton3,mbLeft,[],0,0);
  end;
end;

procedure TFormMain.Edit1Click(Sender: TObject);
begin
(Sender as TEdit).SelectAll;
end;

procedure TFormMain.Edit2Change(Sender: TObject);
begin
if Timer1.Enabled then
  begin
  FlatButton_startMouseDown(FlatButton_start,mbLeft,[],0,0);
  end;
end;

end.
